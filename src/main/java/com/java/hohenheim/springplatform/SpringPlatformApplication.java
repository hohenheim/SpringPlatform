package com.java.hohenheim.springplatform;

import com.github.lianjiatech.retrofit.spring.boot.annotation.RetrofitScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Hohenheim
 */
@SpringBootApplication
@EnableConfigurationProperties
@EnableAsync
@EnableTransactionManagement
@Import(cn.hutool.extra.spring.SpringUtil.class)
@MapperScan("com.java.hohenheim.springplatform.db.mapper")
@RetrofitScan("com.java.hohenheim.springplatform")
@EnableCaching
public class SpringPlatformApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringPlatformApplication.class, args);
    }
}