package com.java.hohenheim.springplatform.utils;

import org.springframework.beans.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hohenheim
 * @date 2020/1/9
 * @description 数据模型操作工具类
 */
public class ModelUtils {
    /**
     * 批量复制实体对象的属性
     * @param tagClass 需要新创建的实体
     * @param sourceList 数据来源实体列表
     * @param opt 实体创建回调
     *
     * @return 新数据实体对象列表
     */
    public static <T> List<T> createModelListByEntity(Class<T> tagClass, List<?> sourceList, CreateModelOpt<T> opt) {
        List<T> modelList = new ArrayList<>();
        try {
            for(Object obj : sourceList) {
                T model = tagClass.getDeclaredConstructor().newInstance();
                BeanUtils.copyProperties(obj, model);
                modelList.add(model);

                if(null != opt) {
                    opt.createdModel(model);
                }
            }
        }
        catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return modelList;
    }

    public interface CreateModelOpt<T> {
        /**
         * 返回新创建的实体对象
         * @param model 新创建的实体
         */
        void createdModel(T model);
    }
}