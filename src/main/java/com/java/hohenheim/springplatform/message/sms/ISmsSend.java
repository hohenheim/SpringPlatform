package com.java.hohenheim.springplatform.message.sms;

/**
 * @author Hohenheim
 * @date 2020/8/9
 * @description 短信发送接口
 */
public interface ISmsSend {
    /**
     * 发送验证码短信
     * @param phone 手机号码
     * @param code 验证码
     * @return 发送是否成功
     */
    boolean sendVerificationCodeSms(String phone, String code);
}