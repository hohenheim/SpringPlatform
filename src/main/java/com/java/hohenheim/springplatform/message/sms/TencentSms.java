package com.java.hohenheim.springplatform.message.sms;

import org.springframework.stereotype.Component;

/**
 * @author Hohenheim
 * @date 2020/8/9
 * @description
 */
@Component("tencentSMS")
public class TencentSms implements ISmsSend {
    @Override
    public boolean sendVerificationCodeSms(String phone, String code) {
        return false;
    }
}
