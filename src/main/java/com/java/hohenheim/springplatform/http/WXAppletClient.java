package com.java.hohenheim.springplatform.http;

import com.github.lianjiatech.retrofit.spring.boot.annotation.RetrofitClient;
import com.java.hohenheim.springplatform.data.model.account.wx.WXAppletSessionRespModel;
import org.slf4j.event.Level;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Hohenheim
 * @date 2020/8/10
 * @description 微信小程序平台接口
 */
@RetrofitClient(baseUrl = "${wechat.applet.baseUrl}", poolName = "thirdPlatform",
        connectTimeoutMs = 18000, readTimeoutMs = 15000, writeTimeoutMs = 15000, logLevel = Level.WARN)
public interface WXAppletClient {
    /**
     * 获取微信小程 session
     * @param appId 微信开放平台ID
     * @param secret 微信开放平台秘钥
     * @param code 用户授权码
     * @param grantType 授权类型，此处只需填写 authorization_code
     * @return
     */
    @GET("/sns/jscode2session")
    WXAppletSessionRespModel getWeChatAppletSession(@Query("appid") String appId, @Query("secret") String secret,
                                                    @Query("js_code") String code, @Query("grant_type") String grantType);
}