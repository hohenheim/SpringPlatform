package com.java.hohenheim.springplatform.web.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Hohenheim
 * @date 2020/8/24
 * @description
 */
@Component
@ConfigurationProperties("token.user")
@Data
public class TokenConfig {
    private long expireSeconds;

    private String signKey;

    private String subject;

    private String issuer;
}
