package com.java.hohenheim.springplatform.web.controller;

import com.github.pagehelper.PageInfo;
import com.java.hohenheim.springplatform.data.model.account.ManagerUserInfoModel;
import com.java.hohenheim.springplatform.data.web.PageInfoModel;
import com.java.hohenheim.springplatform.data.web.account.UserInfoListFilter;
import com.java.hohenheim.springplatform.db.entity.AccountEntity;
import com.java.hohenheim.springplatform.utils.ModelUtils;
import com.java.hohenheim.springplatform.web.service.ManagerAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Hohenheim
 * @date 2020/8/16
 * @description 用户管理系统接口
 */
@Api(tags = "用户管理系统相关接口")
@RestController
@RequestMapping("/m/account")
public class ManagerAccountController {
    @Autowired
    private ManagerAccountService mAccountService;

    @ApiOperation(value = "获取用户数据列表", produces = "application/json")
    @ApiImplicitParam(name = "filter", value = "过滤条件", dataType = "UserInfoListFilter", required = true)
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_MANAGER"}, logical = Logical.OR)
    @PostMapping("/users")
    public PageInfoModel<ManagerUserInfoModel> getUserInfoList(@RequestBody UserInfoListFilter filter) {
        PageInfo<AccountEntity> entityPageInfo = mAccountService.getUserInfoList(filter);

        PageInfoModel<ManagerUserInfoModel> pageInfoModel = new PageInfoModel<>();
        BeanUtils.copyProperties(entityPageInfo, pageInfoModel);

        List<ManagerUserInfoModel> userInfoList = ModelUtils.createModelListByEntity(ManagerUserInfoModel.class,
                entityPageInfo.getList(), null);
        pageInfoModel.setDataList(userInfoList);

        return pageInfoModel;
    }
}