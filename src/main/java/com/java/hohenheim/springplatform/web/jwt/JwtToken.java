package com.java.hohenheim.springplatform.web.jwt;

import lombok.Data;

import java.util.Date;

/**
 * @author Hohenheim
 * @date 2020/8/26
 * @description Token 数据
 */
@Data
public class JwtToken {
    private String subject;

    private String issuer;

    private Date issuedAt;

    private Date expirationDate;
}