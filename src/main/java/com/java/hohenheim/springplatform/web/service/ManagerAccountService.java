package com.java.hohenheim.springplatform.web.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.java.hohenheim.springplatform.data.web.account.UserInfoListFilter;
import com.java.hohenheim.springplatform.db.dao.AccountDAO;
import com.java.hohenheim.springplatform.db.entity.AccountEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Hohenheim
 * @date 2020/8/17
 * @description 用户管理系统服务
 */
@Service
public class ManagerAccountService {
    @Autowired
    private AccountDAO mAccountDAO;

    public PageInfo<AccountEntity> getUserInfoList(UserInfoListFilter filter) {
        QueryWrapper<AccountEntity> queryWrapper = new QueryWrapper<>();
        if(null != filter.getUserId()) {
            queryWrapper.eq("user_id", filter.getUserId());
        }
        if(null != filter.getStatus()) {
            queryWrapper.eq("status", filter.getStatus());
        }
        if(null != filter.getSex()) {
            queryWrapper.eq("sex", filter.getSex());
        }
        if(StringUtils.isNotBlank(filter.getUserName())) {
            queryWrapper.eq("user_name", filter.getUserName());
        }
        if(StringUtils.isNotBlank(filter.getPhone())) {
            queryWrapper.eq("phone", filter.getPhone());
        }
        if(StringUtils.isNotBlank(filter.getEmail())) {
            queryWrapper.eq("email", filter.getEmail());
        }

        PageHelper.startPage(filter.getPageNum(), filter.getPageSize());
        List<AccountEntity> accountList = mAccountDAO.list(queryWrapper);

        return new PageInfo<>(accountList);
    }
}
