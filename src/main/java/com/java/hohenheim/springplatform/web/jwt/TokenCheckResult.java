package com.java.hohenheim.springplatform.web.jwt;

import com.java.hohenheim.springplatform.define.ResultCodes;
import lombok.Data;

/**
 * @author Hohenheim
 * @date 2020/8/25
 * @description Token校验结果
 */
@Data
public class TokenCheckResult<T extends JwtToken> {
    private T jwtToken;

    private boolean checkResult;

    private ResultCodes result;
}