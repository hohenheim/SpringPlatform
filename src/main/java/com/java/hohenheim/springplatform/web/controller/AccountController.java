package com.java.hohenheim.springplatform.web.controller;

import cn.hutool.core.lang.Validator;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import com.java.hohenheim.springplatform.data.model.account.LoginParams;
import com.java.hohenheim.springplatform.data.model.account.LoginResultModel;
import com.java.hohenheim.springplatform.data.model.account.UserInfoModel;
import com.java.hohenheim.springplatform.data.web.account.RegisterParams;
import com.java.hohenheim.springplatform.db.entity.AccountEntity;
import com.java.hohenheim.springplatform.define.ResultCodes;
import com.java.hohenheim.springplatform.exception.PlatformException;
import com.java.hohenheim.springplatform.web.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author Hohenheim
 * @date 2020/7/15
 * @description 账户系统 Controller
 */
@Api(tags = "账户相关接口")
@RestController
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountService mAccountService;

    @GetMapping("/register/vercode/{phone}")
    @ApiOperation(value = "手机验证码获取", produces = "application/json")
    @ApiOperationSupport(order = 1)
    @ApiImplicitParam(name = "phone", value = "手机号码", required = true)
    public boolean createPhoneRegisterVerCode(@PathVariable("phone") String phone) {
        boolean saveResult = false;

        if (!Validator.isMobile(phone)) {
            throw new PlatformException(ResultCodes.PHONE_FORMAT_ERROR);
        }
        else {
            saveResult = mAccountService.createPhoneRegisterVerCode(phone);
            if (!saveResult) {
                throw new PlatformException(ResultCodes.REGISTER_VERIFICATION_CODE_SAVE_FAIL);
            }
        }

        return saveResult;
    }

    @PostMapping("/register/{registerType}")
    @ApiOperation(value = "用户注册", produces = "application/json")
    @ApiOperationSupport(order = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "param", value = "账户注册参数", dataType = "RegisterParams", required = true),
            @ApiImplicitParam(name = "registerType", value = "注册类型，phone：手机号码注册；email：邮件注册；name：用户名注册", required = true)
    })
    public boolean register(@RequestBody RegisterParams param, @PathVariable("registerType") String registerType) {
        return mAccountService.register(param, registerType);
    }

    @PostMapping("/login/{loginType}")
    @ApiOperation(value = "用户登录", produces = "application/json")
    @ApiOperationSupport(order = 3)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "param", value = "登录参数", dataType = "LoginParams", required = true),
            @ApiImplicitParam(name = "loginType", value = "登录类型，phone：手机登录；email：邮件登录；name：用户名登录；" +
                    "wxApplet：微信小程序", required = true)
    })
    public LoginResultModel login(@RequestBody @Validated LoginParams param, @PathVariable("loginType") String loginType) {
        return mAccountService.login(param, loginType);
    }

    @ApiOperation(value = "根据用户ID，获取用户信息", produces = "application/json")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true)
    @ApiOperationSupport(order = 4)
    @GetMapping("/userInfo/{userId}")
    public UserInfoModel getUserInfo(@PathVariable("userId") Long userId) {
        AccountEntity accountEntity = mAccountService.getAccountInfo(userId);
        if(null != accountEntity) {
            UserInfoModel userInfo = new UserInfoModel();
            BeanUtils.copyProperties(accountEntity, userInfo);
            return userInfo;
        }
        else {
            throw new PlatformException(ResultCodes.USER_DOES_NOT_EXIST);
        }
    }
}