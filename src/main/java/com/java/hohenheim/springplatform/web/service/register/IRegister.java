package com.java.hohenheim.springplatform.web.service.register;

import com.java.hohenheim.springplatform.data.web.account.RegisterParams;

/**
 * @author Hohenheim
 * @date 2020/8/8
 * @description 注册业务处理接口
 */
public interface IRegister {
    /**
     * 注册模块
     * @param registerParams 注册相关参数
     */
    boolean register(RegisterParams registerParams);
}