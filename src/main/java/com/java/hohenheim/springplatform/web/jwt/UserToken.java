package com.java.hohenheim.springplatform.web.jwt;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Hohenheim
 * @date 2020/8/26
 * @description 用户Token
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserToken extends JwtToken {
    private long userId;
}