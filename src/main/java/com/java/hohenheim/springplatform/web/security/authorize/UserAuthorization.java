package com.java.hohenheim.springplatform.web.security.authorize;

import com.java.hohenheim.springplatform.web.jwt.UserToken;
import lombok.AllArgsConstructor;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author Hohenheim
 * @date 2020/9/4
 * @description
 */
@AllArgsConstructor
public class UserAuthorization implements AuthenticationToken {
    private UserToken userToken;

    @Override
    public Object getPrincipal() {
        return userToken;
    }

    @Override
    public Object getCredentials() {
        return userToken.getUserId();
    }
}