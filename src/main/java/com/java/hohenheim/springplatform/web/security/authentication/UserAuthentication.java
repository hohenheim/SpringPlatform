package com.java.hohenheim.springplatform.web.security.authentication;

import lombok.AllArgsConstructor;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author LiaoJian
 */
@AllArgsConstructor
public class UserAuthentication implements AuthenticationToken {
    private String token;

    @Override
    public Object getPrincipal() {
        return this.token;
    }

    @Override
    public Object getCredentials() {
        return this.token;
    }
}