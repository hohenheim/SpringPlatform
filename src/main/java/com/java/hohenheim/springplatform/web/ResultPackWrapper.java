package com.java.hohenheim.springplatform.web;

import com.java.hohenheim.springplatform.data.web.FailRespModel;
import com.java.hohenheim.springplatform.data.web.SuccessRespModel;
import com.java.hohenheim.springplatform.define.ResultCodes;

/**
 * @author Hohenheim
 * @date 2019/12/22
 * @description 响应数据封装
 */
public class ResultPackWrapper {
    public static SuccessRespModel<Object> reqSuccess() {
        return reqSuccess(null);
    }

    public static SuccessRespModel<Object> reqSuccess(Object data) {
        return reqSuccess(data, ResultCodes.RETURN_SUCCESS.getMsg());
    }

    public static SuccessRespModel<Object> reqSuccess(Object data, String successMsg) {
        SuccessRespModel<Object> resultResp = new SuccessRespModel<>();
        resultResp.setResultCode(ResultCodes.RETURN_SUCCESS.getCode());
        resultResp.setMsg(successMsg);
        resultResp.setReqSuccess(true);

        if(null != data) {
            resultResp.setData(data);
        }

        return resultResp;
    }

    public static FailRespModel<Object> reqFail(ResultCodes resultCode) {
        return reqFail(resultCode.getCode(), resultCode.getMsg());
    }

    public static FailRespModel<Object> reqFail(int errCode, String msg) {
        FailRespModel<Object> resultResp = new FailRespModel<>();
        resultResp.setResultCode(errCode);
        resultResp.setMsg(msg);
        resultResp.setReqSuccess(false);

        return resultResp;
    }
}