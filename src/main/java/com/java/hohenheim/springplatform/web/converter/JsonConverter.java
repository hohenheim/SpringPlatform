package com.java.hohenheim.springplatform.web.converter;

import com.google.common.collect.Lists;
import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import com.java.hohenheim.springplatform.data.exception.ExceptionPackingModel;
import com.java.hohenheim.springplatform.data.web.BaseRespModel;
import com.java.hohenheim.springplatform.define.ResultCodes;
import com.java.hohenheim.springplatform.web.ResultPackWrapper;
import com.java.hohenheim.springplatform.web.handler.PlatformErrorAttributes;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Hohenheim
 * @date 2020/7/11
 * @description
 */
public class JsonConverter extends MappingJackson2HttpMessageConverter {
    /**
     * 需要添加元数据的包
     */
    private final List<String> mToWrapPackages = Lists.newArrayList("java.util",
            "java.lang",
            "com.github.pagehelper",
            "com.baomidou.mybatisplus.extension.plugins.pagination",
            "com.java.hohenheim.springplatform");

    @Override
    protected void writeInternal(Object object, Type type, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        // 判断是否需要再包一层
        boolean toWrap = false;
        for (String mPackage : mToWrapPackages) {
            if (objInPackage(object, mPackage)) {
                toWrap = true;
            }
        }
        if (!toWrap) {
            super.writeInternal(object, type, outputMessage);
            return;
        }


        Object respData = null;
        if (object instanceof Map) {
            Map objMap = (Map) object;
            Object errInfo = objMap.get(PlatformErrorAttributes.PLATFORM_ERROR_ATTR_KEY);
            if (errInfo instanceof ExceptionPackingModel) {
                //错误信息处理
                respData = errInfo;
            }
        }

        JsonConverterExec jsonConverter = object.getClass().getAnnotation(JsonConverterExec.class);
        if (null != jsonConverter) {
            if (!jsonConverter.packing()) {
                respData = object;
            }
            else if (jsonConverter.successPacking()) {
                respData = ResultPackWrapper.reqSuccess(object);
            }
            else {
                BaseRespModel<Object> respModel = ResultPackWrapper.reqFail(ResultCodes.REQ_OPT_FAILURE);
                respModel.setData(object);
                respData = respModel;
            }
        }
        else if(object instanceof Boolean) {
            boolean value = (Boolean) object;
            if(value) {
                respData = ResultPackWrapper.reqSuccess();
            }
            else {
                respData = ResultPackWrapper.reqFail(ResultCodes.REQ_OPT_FAILURE);
            }
        }
        else if(null == respData){
            respData = ResultPackWrapper.reqSuccess(object);
        }

        super.writeInternal(respData, type, outputMessage);
    }

    /** 判断对象是否来自某个包 */
    private boolean objInPackage(Object object, String packageToCheck) {
        if (object instanceof Collection<?>) {
            if (0 == ((Collection<?>) object).size()) {
                return true;
            }
            for (Object elem : (Collection<?>) object) {
                String currPackage = elem.getClass().getPackage().getName();
                if (currPackage.contains(packageToCheck)) {
                    return true;
                }
            }
        }
        else { // 单个对象
            String currPackage = object.getClass().getPackage().getName();
            return currPackage.contains(packageToCheck);
        }
        return false;
    }
}
