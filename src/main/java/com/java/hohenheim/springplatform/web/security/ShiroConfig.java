package com.java.hohenheim.springplatform.web.security;

import com.java.hohenheim.springplatform.web.security.authentication.UserAuthenticationRealm;
import com.java.hohenheim.springplatform.web.security.authentication.UserAuthenticationFilter;
import com.java.hohenheim.springplatform.web.security.authorize.UserAuthorizationRealm;
import com.java.hohenheim.springplatform.web.security.authorize.UserAuthorizeFilter;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LiaoJian, Hohenheim
 * @description Shiro 配置器
 */
@Configuration
public class ShiroConfig {
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    @DependsOn("lifecycleBeanPostProcessor")
    public static DefaultAdvisorAutoProxyCreator getLifecycleBeanPostProcessor() {
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        // 强制使用cglib
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    /**
     * 开启shiro aop注解支持.
     * @param securityManager Shiro管理器
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(
            @Qualifier("securityManager") SecurityManager securityManager) {

        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    @Bean("securityManager")
    public SecurityManager securityManager(@Qualifier("userAuthenticationRealm") UserAuthenticationRealm authRealm,
                                           @Qualifier("userAuthorizationRealm") UserAuthorizationRealm authorRealm) {
        List<Realm> realmList = new ArrayList<>();
        realmList.add(authRealm);
        realmList.add(authorRealm);

        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealms(realmList);

        //关闭shiro自带的session
        DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
        defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
        subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
        securityManager.setSubjectDAO(subjectDAO);

        return securityManager;
    }

    @Bean
    public ShiroFilterFactoryBean shirFilter(@Qualifier("securityManager") SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        LinkedHashMap<String, Filter> filterMap = new LinkedHashMap<>();
        filterMap.put("authenu", new UserAuthenticationFilter());
        filterMap.put("authoru", new UserAuthorizeFilter());
        shiroFilterFactoryBean.setFilters(filterMap);

        Map<String, String> filterChain = new LinkedHashMap<>();
        // 放行路径
        filterChain.put("/doc.html/**", "anon");
        filterChain.put("/swagger-ui.html", "anon");
        filterChain.put("/swagger-resources/**", "anon");
        filterChain.put("/v2/**", "anon");
        filterChain.put("/webjars/**", "anon");
        filterChain.put("/images/**", "anon");
        filterChain.put("/client/rsa/**", "anon");
        filterChain.put("/client/account/signIn", "anon");
        filterChain.put("/client/account/signUp", "anon");
        filterChain.put("/client/account/sendCaptcha", "anon"); // 短信发送
        filterChain.put("/manager/login", "anon");
        filterChain.put("/register/**", "anon");
        filterChain.put("/login/**", "anon");
        filterChain.put("/test/**", "anon");
        filterChain.put("/druid/**", "anon");
        filterChain.put("/account/userInfo/**", "authenu");
        filterChain.put("/account/**", "anon");
        filterChain.put("/m/account/**", "authenu,authoru");
        // 其余路径均被jwt拦截器拦截
        filterChain.put("/**", "authenu");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChain);
        return shiroFilterFactoryBean;
    }

    @Bean
    @ConditionalOnMissingBean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator app = new DefaultAdvisorAutoProxyCreator();
        app.setProxyTargetClass(true);
        return app;
    }
}
