package com.java.hohenheim.springplatform.web.service.login;

import com.java.hohenheim.springplatform.data.model.account.LoginParams;
import com.java.hohenheim.springplatform.data.model.account.UserInfoModel;

/**
 * @author Hohenheim
 * @date 2020/8/11
 * @description
 */
public interface ILogin {
    /**
     * 登录
     * @param param 登录参数
     * @return 用户信息
     */
    UserInfoModel login(LoginParams param);
}