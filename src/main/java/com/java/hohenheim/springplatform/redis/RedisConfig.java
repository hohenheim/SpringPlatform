package com.java.hohenheim.springplatform.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.java.hohenheim.springplatform.db.entity.AccountEntity;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.*;

import java.time.Duration;


/**
 * @author Hohenheim
 * @date 2020/7/29
 * @description Redis 配置
 */
@Configuration
public class RedisConfig {
    private StringRedisSerializer mStringRedisSerializer;

    public RedisConfig() {
        mStringRedisSerializer = new StringRedisSerializer();
    }

    @Bean("RegisterVerificationCodeRedis")
    public RedisTemplate<String, String> registerVerificationCodeRedis(RedisConnectionFactory connFactory) {
        RedisTemplate<String, String> template = new RedisTemplate<>();
        template.setConnectionFactory(connFactory);
        template.setKeySerializer(mStringRedisSerializer);
        template.setValueSerializer(mStringRedisSerializer);

        return template;
    }

    @Bean("RegisterUserInfoRedis")
    public RedisTemplate<String, AccountEntity> registerUserInfoRedis(RedisConnectionFactory connFactory) {

        RedisTemplate<String, AccountEntity> template = new RedisTemplate<>();
        template.setConnectionFactory(connFactory);
        Jackson2JsonRedisSerializer<AccountEntity> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(AccountEntity.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 解决jackson2无法反序列化LocalDateTime的问题
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        om.registerModule(new JavaTimeModule());
        jackson2JsonRedisSerializer.setObjectMapper(om);
        //键序列化
        template.setKeySerializer(mStringRedisSerializer);
        template.setHashKeySerializer(mStringRedisSerializer);
        //值序列化
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        return template;
    }

    @Primary
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory){
        //缓存配置对象
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig();
        //设置缓存的默认超时时间：30分钟
        redisCacheConfiguration = redisCacheConfiguration.entryTtl(Duration.ofMinutes(30L))
                //如果是空值，不缓存
                .disableCachingNullValues()
                //设置key序列化器
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(keySerializer()))
                //设置value序列化器
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer((valueSerializer())));
        return RedisCacheManager
                .builder(RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory))
                .cacheDefaults(redisCacheConfiguration).build();
    }

    private RedisSerializer<String> keySerializer() {
        return new StringRedisSerializer();
    }

    private RedisSerializer<AccountEntity> valueSerializer() {
        Jackson2JsonRedisSerializer<AccountEntity> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(AccountEntity.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 解决jackson2无法反序列化LocalDateTime的问题
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        om.registerModule(new JavaTimeModule());
        jackson2JsonRedisSerializer.setObjectMapper(om);
        return jackson2JsonRedisSerializer;
    }
}