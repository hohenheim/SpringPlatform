package com.java.hohenheim.springplatform.redis.ops;

import io.lettuce.core.RedisException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author Hohenheim
 * @date 2020/7/29
 * @description 注册验证码 Redis 操作
 */
@Component
@Slf4j
public class RegisterVerificationCodeRedisOps {
    private final String KEY_SUFFIX = "account:code:phone:";
    private final String SEND_KEY_SUFFIX = "account:code:send:";

    private final int TIME_OUT_MINUTES = 3; //注册验证码超时时间，3分钟
    private final int SEND_TIME_OUT_MINUTES = 1; //已发送注册验证码超时时间，1分钟

    @Resource(name = "RegisterVerificationCodeRedis")
    private RedisTemplate<String, String> mVerificationCodeRedis;

    /**
     * 保存手机验证码
     * @param phone 手机号码
     * @param code 验证码
     * @return true：保存成功；false：保存失败
     */
    public boolean saveVerificationCode(String phone, String code) {
        boolean saveResult = false;
        String key = KEY_SUFFIX + phone;

        try {
            mVerificationCodeRedis.opsForValue().set(key, code, TIME_OUT_MINUTES, TimeUnit.MINUTES);
            saveResult = true;
        }
        catch (RedisException e) {
            String logMsg = "保存手机 " + phone + " 验证码 " + code + " 失败";
            log.error("[Redis]", logMsg, e);
        }

        return saveResult;
    }

    /**
     * 获取手机验证码
     * @param phone 手机号码
     * @return 验证码
     */
    public String getVerificationCode(String phone) {
        String key = KEY_SUFFIX + phone;
        return mVerificationCodeRedis.opsForValue().get(key);
    }

    /**
     * 删除手机验证码
     * @param phone 手机号码
     * @return true：删除成功；false：删除失败
     */
    public boolean delVerificationCode(String phone) {
        String key = KEY_SUFFIX + phone;
        Boolean delResult = mVerificationCodeRedis.delete(key);

        return null==delResult ? false : delResult;
    }

    /**
     * 保存已发送的手机验证码
     * @param phone 手机号码
     * @param code 验证码
     * @return true：保存成功；false：保存失败
     */
    public boolean saveBeenSendCode(String phone, String code) {
        boolean saveResult = false;

        String key = SEND_KEY_SUFFIX + phone;

        try {
            mVerificationCodeRedis.opsForValue().set(key, code, SEND_TIME_OUT_MINUTES, TimeUnit.MINUTES);
            saveResult = true;
        }
        catch (RedisException e) {
            String logMsg = "保存手机 " + phone + " 发送验证码 " + code + " 的信息失败";
            log.error("[Redis]", logMsg, e);
        }

        return saveResult;
    }

    /**
     * 获取已发送的手机验证码
     * @param phone 手机号码
     * @return 验证码
     */
    public String getBeenSendCode(String phone) {
        String key = SEND_KEY_SUFFIX + phone;
        return mVerificationCodeRedis.opsForValue().get(key);
    }

    /**
     * 删除已发送的手机验证码
     * @param phone 手机号码
     * @return true：删除成功；false：删除失败
     */
    public boolean delBeenSendCode(String phone) {
        String key = SEND_KEY_SUFFIX + phone;
        Boolean delResult = mVerificationCodeRedis.delete(key);

        return null==delResult ? false : delResult;
    }
}