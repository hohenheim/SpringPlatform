package com.java.hohenheim.springplatform.redis.ops;


import com.java.hohenheim.springplatform.db.entity.RoleEntity;
import io.lettuce.core.RedisException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class RoleDataRedisOps {

    private final String KEY_SUFFIX = "role:code:";

    private final int TIME_OUT_DAY = 1; //超时时间，1天


}
