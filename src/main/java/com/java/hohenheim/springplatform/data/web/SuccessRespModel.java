package com.java.hohenheim.springplatform.data.web;

import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Hohenheim
 * @date 2020/2/6
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonConverterExec(packing = false)
public class SuccessRespModel<T> extends BaseRespModel<T> {
}