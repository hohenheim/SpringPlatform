package com.java.hohenheim.springplatform.data.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Hohenheim
 * @date 2020/8/17
 * @description 分页信息
 */
@ApiModel(description = "分页数据")
@Data
public class PageInfoModel<T> {
    @ApiModelProperty(value = "每页的数量")
    private int pageSize;

    @ApiModelProperty(value = "总页数")
    private int pages;

    @ApiModelProperty(value = "当前页")
    private int pageNum;

    @ApiModelProperty(value = "当页数据的数量")
    private int size;

    @ApiModelProperty(value = "数据列表")
    private List<T> dataList;
}