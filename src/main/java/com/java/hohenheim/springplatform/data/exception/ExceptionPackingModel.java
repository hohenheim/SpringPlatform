package com.java.hohenheim.springplatform.data.exception;

import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import com.java.hohenheim.springplatform.define.ResultCodes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Hohenheim
 * @date 2020/1/14
 * @description MVC错误信息拓展
 */
@Data
@JsonConverterExec(successPacking = false)
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionPackingModel {
    private ExceptionInfoModel exceptionInfo;

    private int code;

    private String message;

    public ExceptionPackingModel(ResultCodes resultCodes, ExceptionInfoModel exceptionInfo) {
        this.exceptionInfo = exceptionInfo;
        this.code = resultCodes.getCode();
        this.message = resultCodes.getMsg();
    }
}
