package com.java.hohenheim.springplatform.data.web.account;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Hohenheim
 * @date 2020/7/19
 * @description 用户注册参数
 */
@ApiModel(description = "用户注册参数")
@Data
public class RegisterParams {
    @ApiModelProperty(value = "注册账户", required = true)
    private String account;

    @ApiModelProperty(value = "验证码", required = true)
    private String verCode;

    @ApiModelProperty(value = "密码", required = true)
    private String pwd;
}