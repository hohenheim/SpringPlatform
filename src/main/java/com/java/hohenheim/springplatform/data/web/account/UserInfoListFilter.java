package com.java.hohenheim.springplatform.data.web.account;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;

/**
 * @author Hohenheim
 * @date 2020/8/17
 * @description 用户列表过滤条件
 */
@ApiModel(value = "UserInfoListFilter", description = "用户列表过滤条件")
@Data
public class UserInfoListFilter {
    @ApiModelProperty(value = "页长，默认30", required = true)
    @Min(value = 1, message = "页长不能为0")
    private int pageSize = 30;

    @ApiModelProperty(value = "页码，第一页为1", required = true)
    @Min(value = 1, message = "页码不能为0")
    private int pageNum = 1;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "用户手机")
    private String phone;

    @ApiModelProperty(value = "用户ID")
    private String email;

    @ApiModelProperty(value = "性别")
    private Short sex;

    @ApiModelProperty(value = "状态")
    private Short status;
}