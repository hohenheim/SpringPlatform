package com.java.hohenheim.springplatform.data.model.account;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Hohenheim
 * @date 2020/8/11
 * @description 登录参数
 */
@ApiModel(description = "登录参数")
@Data
public class LoginParams {
    @ApiModelProperty(name = "用户账号（用户名，手机号码、邮箱），如果是第三方平台登录，填上第三方平台授权码", required = true)
    @NotBlank(message = "用户名不能为空")
    private String account;

    @ApiModelProperty(name = "账号密码")
    private String password;
}