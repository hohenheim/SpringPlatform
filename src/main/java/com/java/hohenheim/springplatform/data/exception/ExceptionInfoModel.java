package com.java.hohenheim.springplatform.data.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import lombok.Data;

import java.util.Date;

/**
 * @author Hohenheim
 * @date 2020/1/14
 * @description MVC错误信息实体
 */
@Data
@JsonConverterExec(successPacking = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExceptionInfoModel {
    private Date timestamp;

    private int status;

    private String error;

    private String message;

    private String path;
}