package com.java.hohenheim.springplatform.data.model.account;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Hohenheim
 * @date 2020/7/19
 * @description 用户基础数据实体类
 */
@ApiModel(description = "用户数据")
@JsonConverterExec
@Data
public class UserInfoModel {
    @ApiModelProperty(value = "用户ID")
    @JsonSerialize(using=ToStringSerializer.class)
    private long userId;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "用户头像URL")
    private String avatar;

    @ApiModelProperty(value = "用户性别，0.女；1.男；2.无性别意识")
    private short sex;

    @ApiModelProperty(value = "出生年月")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "地区")
    private String area;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "地址")
    private String address;
}