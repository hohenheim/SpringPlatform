package com.java.hohenheim.springplatform.data.model.account;

import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author Hohenheim
 * @date 2020/8/16
 * @description 用户管理系统获取到的用户数据
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户数据（用户管理系统）")
@JsonConverterExec
@Data
public class ManagerUserInfoModel extends UserInfoModel {
    @ApiModelProperty(value = "账号状态：0.封禁；1.有效")
    private Short status;

    @ApiModelProperty(value = "注册日期")
    private LocalDateTime createTime;
}