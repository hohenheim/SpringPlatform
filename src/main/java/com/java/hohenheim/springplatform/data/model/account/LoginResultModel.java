package com.java.hohenheim.springplatform.data.model.account;


import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Hohenheim
 * @date 2020/8/16
 * @description 登录结果
 */
@ApiModel(description = "登录结果")
@JsonConverterExec
@Data
public class LoginResultModel {
    @ApiModelProperty(value = "用户数据")
    private UserInfoModel userInfo;

    @ApiModelProperty(value = "登录凭证")
    private String token;
}
