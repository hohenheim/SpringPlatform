package com.java.hohenheim.springplatform.data.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Hohenheim
 * @date 2018/4/27
 * @description 响应数据统一封装
 */
@ApiModel(description = "统一封装数据")
@Data
public class BaseRespModel<T> {
    @ApiModelProperty("业务是否处理成功")
    private boolean reqSuccess;
    @ApiModelProperty("业务响应代码")
    private int resultCode;
    @ApiModelProperty("响应信息")
    private String msg;

    @ApiModelProperty("响应数据")
    private T data;
}