package com.java.hohenheim.springplatform.data.web.account;

import com.java.hohenheim.springplatform.anno.JsonConverterExec;
import io.swagger.annotations.ApiModel;

/**
 * @author Hohenheim
 * @date 2020/7/19
 * @description
 */
@ApiModel
@JsonConverterExec
public class AccountRegisterResp {
}