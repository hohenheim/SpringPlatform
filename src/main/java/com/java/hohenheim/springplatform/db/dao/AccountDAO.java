package com.java.hohenheim.springplatform.db.dao;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.hohenheim.springplatform.db.entity.AccountEntity;
import com.java.hohenheim.springplatform.db.mapper.AccountMapper;
import org.springframework.stereotype.Service;

/**
 * @author Hohenheim
 * @date 2020/7/16
 * @description 账户信息表操作方法
 */
@Service
public class AccountDAO extends ServiceImpl<AccountMapper, AccountEntity> {

}