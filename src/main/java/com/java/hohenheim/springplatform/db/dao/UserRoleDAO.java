package com.java.hohenheim.springplatform.db.dao;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.hohenheim.springplatform.db.entity.UserRoleEntity;
import com.java.hohenheim.springplatform.db.mapper.UserRoleMapper;
import org.springframework.stereotype.Service;

/**
 * @author Hohenheim
 * @date 2020/8/9
 * @description
 */
@Service
public class UserRoleDAO extends ServiceImpl<UserRoleMapper, UserRoleEntity> {
}
