package com.java.hohenheim.springplatform.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.hohenheim.springplatform.db.entity.AccountEntity;
import com.java.hohenheim.springplatform.db.entity.AccountWithThirdEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Hohenheim
 */
@Repository
public interface AccountMapper extends BaseMapper<AccountEntity> {
    int insertSelective(AccountEntity record);

    int updateByPrimaryKeySelective(AccountEntity record);

    /**
     * 根据第三方平台类型和 OpenId，查询含第三方平台信息的用户信息
     * @param platform 平台类型编号
     * @param openId 第三方平台用户唯一标识
     * @return 含第三方平台信息的用户数据
     */
    AccountWithThirdEntity selectWithThirdByOpenId(@Param("platform") int platform, @Param("openId") String openId);
}