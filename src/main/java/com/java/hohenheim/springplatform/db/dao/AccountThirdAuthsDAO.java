package com.java.hohenheim.springplatform.db.dao;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.hohenheim.springplatform.db.entity.AccountThirdAuthsEntity;
import com.java.hohenheim.springplatform.db.mapper.AccountThirdAuthsMapper;
import org.springframework.stereotype.Service;

/**
 * @author Hohenheim
 * @date 2020/7/16
 * @description 第三方平台账户信息表操作方法
 */
@Service
public class AccountThirdAuthsDAO extends ServiceImpl<AccountThirdAuthsMapper, AccountThirdAuthsEntity> {
}
