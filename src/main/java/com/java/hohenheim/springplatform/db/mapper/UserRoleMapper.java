package com.java.hohenheim.springplatform.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.hohenheim.springplatform.db.entity.UserRoleEntity;
import com.java.hohenheim.springplatform.db.entity.UserRoleWithRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Hohenheim
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRoleEntity> {
    int insertSelective(UserRoleEntity record);

    int updateByPrimaryKeySelective(UserRoleEntity record);

    /**
     * 查询用户角色信息
     * @param userId 用户ID
     * @return 用户角色信息
     */
    UserRoleWithRole getUserRoles(@Param("userId") long userId);
}