package com.java.hohenheim.springplatform.db.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Hohenheim
 * @date 2020/8/12
 * @description 用户第三方平台信息，与用户信息关联表
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AccountWithThirdEntity extends AccountEntity {
    private AccountThirdAuthsEntity thirdEntity;
}