package com.java.hohenheim.springplatform.db.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author Hohenheim
 * @date 2020/9/7
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserRoleWithRole extends UserRoleEntity {
    private static final long serialVersionUID = -1348069490276101308L;

    private List<RoleEntity> roleList;
}