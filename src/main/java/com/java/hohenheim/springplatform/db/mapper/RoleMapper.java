package com.java.hohenheim.springplatform.db.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.hohenheim.springplatform.db.entity.RoleEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleMapper  extends BaseMapper<RoleEntity> {


    int insertSelective(RoleEntity record);

    int updateByPrimaryKeySelective(RoleEntity record);
}
