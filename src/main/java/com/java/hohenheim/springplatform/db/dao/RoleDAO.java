package com.java.hohenheim.springplatform.db.dao;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.hohenheim.springplatform.db.entity.RoleEntity;
import com.java.hohenheim.springplatform.db.mapper.RoleMapper;
import org.springframework.stereotype.Service;

@Service
public class RoleDAO extends ServiceImpl<RoleMapper, RoleEntity> {
}
