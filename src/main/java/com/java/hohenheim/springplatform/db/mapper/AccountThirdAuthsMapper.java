package com.java.hohenheim.springplatform.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.hohenheim.springplatform.db.entity.AccountThirdAuthsEntity;
import org.springframework.stereotype.Repository;

/**
 * @author Hohenheim
 */
@Repository
public interface AccountThirdAuthsMapper extends BaseMapper<AccountThirdAuthsEntity> {
    int insertSelective(AccountThirdAuthsEntity record);

    int updateByPrimaryKeySelective(AccountThirdAuthsEntity record);
}