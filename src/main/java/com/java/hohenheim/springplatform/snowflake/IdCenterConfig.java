package com.java.hohenheim.springplatform.snowflake;

import lombok.Data;

/**
 * @author Hohenheim
 * @date 2020/7/28
 * @description ID 中心配置
 */
@Data
public class IdCenterConfig {
    private int dataCenterId;

    private int machineId;
}