package com.java.hohenheim.springplatform.snowflake;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author Hohenheim
 * @date 2020/7/28
 * @description ID 中心配置工厂
 */
@Configuration
public class IdCenterFactory {
    @Bean("UserIdConfig")
    @ConfigurationProperties("id-center.snowflake.user-id")
    @Primary
    public IdCenterConfig initUserIdConfig() {
        return new IdCenterConfig();
    }

    @Bean("UserId")
    @Primary
    public SnowflakeWorker userIdWorker(@Qualifier("UserIdConfig") IdCenterConfig config) {
        return new SnowflakeWorker(config);
    }
}
