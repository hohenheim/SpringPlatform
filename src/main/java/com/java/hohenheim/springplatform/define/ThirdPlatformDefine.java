package com.java.hohenheim.springplatform.define;

/**
 * @author Hohenheim
 * @date 2020/8/12
 * @description 第三方平台编号
 */
public enum ThirdPlatformDefine {
    /* 第三方平台定义 */
    WX(1, "微信"),
    QQ(2, "QQ"),
    WEIBO(3, "新浪微博");

    ThirdPlatformDefine(int code, String platformName) {
        this.code = code;
        this.platformName = platformName;
    }

    private int code;
    private String platformName;

    public int getCode() {
        return code;
    }

    public String getPlatformName() {
        return platformName;
    }
}