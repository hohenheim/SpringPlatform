package com.java.hohenheim.springplatform.define;

/**
 * @author Hohenheim
 * @date 2020/8/12
 * @description 微信结果代码
 */
public enum  WeChatResultCode {
    /** 响应代码 */
    SYSTEM_BUSY(-1, "系统繁忙，请稍候再试"),
    SUCCESS(0, "请求成功"),
    CODE(40029, "授权码无效"),
    TOO_MANY_REQ(45011, "请求过于频繁，请稍候再试");

    WeChatResultCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private int code;

    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String getDesc(int code) {
        String desc = "";

        WeChatResultCode[] resultArray = values();
        for (WeChatResultCode result : resultArray) {
            if (result.getCode() == code) {
                desc = result.getDesc();
                break;
            }
        }

        return desc;
    }
}