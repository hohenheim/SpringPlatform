package com.java.hohenheim.springplatform.define;

/**
 * @author Hohenheim
 * @date 2020/8/9
 * @description 请求结果代码
 */
public enum ResultCodes {

	/* 通用错误 */
	RETURN_SUCCESS(0, "执行成功"),
	COMMON_SYSTEM_ERROR(1, "服务错误"),
	COMMON_INVALID_PARAMS(2, "参数错误"),
	REQ_OPT_FAILURE(3, "请求处理失败"),
	NOT_LOGGED_ON(4, "没有登录"),
	UNAUTHORIZED(5, "没有授权"),

	/* Token检验 */
	TOKEN_SIGN_EXCEPTION(800,"非法登录凭证"),
	TOKEN_EXPIRED(801,"登录凭证已失效"),

	/* 登录、注册相关信息 */
	PHONE_FORMAT_ERROR(1000, "请输入正确手机号码"),
	REGISTER_VERIFICATION_CODE_SAVE_FAIL(1001, "验证码获取失败，请稍后再次尝试"),
	REGISTER_VERIFICATION_CODE_SEND_BUSY(1002, "验证码已发送，请注意您的手机短信"),
	REGISTER_VERIFICATION_CODE_SEND_FAIL(1003, "验证码发送失败"),
	NO_REGISTER_METHOD(1004, "不支持的注册类型"),

	PHONE_HAS_BEEN_REGISTERED(1005, "该手机号已存在"),
	EMAIL_HAS_BEEN_REGISTERED(1005, "该邮箱已存在"),
	NAME_HAS_BEEN_REGISTERED(1005, "该用户名已存在"),

	VERIFICATION_CODE_MUST_NOT_EMPTY(1006, "请输入验证码"),
	PWD_MUST_NOT_EMPTY(1007, "请输入密码"),
	VERIFICATION_CODE_IS_WRONG(1008, "验证码错误"),
	TOKEN_SING_FAIL(1009, "TOKEN签发失败"),

	REGISTER_FAIL(1019, "注册失败"),

	LOGIN_FAIL(1020, "系统异常，登录失败"),
	NO_LOGIN_METHOD(1021, "非法登录类型"),

	NAME_PWD_MUST_NOT_EMPTY(1022, "用户名和密码不能为空"),
	EMAIL_PWD_MUST_NOT_EMPTY(1022, "邮箱和密码不能为空"),
	PHONE_PWD_MUST_NOT_EMPTY(1022, "手机号码和密码不能为空"),

	USER_DOES_NOT_EXIST(1023, "用户不存在"),
	ACCOUNT_OR_PWD_ERROR(1024, "用户名或密码错误");


	private int code;
	private String msg;

	ResultCodes(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getMsg() {
		return this.msg;
	}

	public Integer getCode() {
		return this.code;
	}
}
