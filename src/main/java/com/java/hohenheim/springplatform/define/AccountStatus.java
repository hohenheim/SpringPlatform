package com.java.hohenheim.springplatform.define;

/**
 * @author Hohenheim
 * @date 2020/8/8
 * @description 账户状态
 */
public enum AccountStatus {
    /* 账号禁用 */
    ACCOUNT_UNABLE((short)0, "账号禁用"),
    /* 账号可用 */
    ACCOUNT_ENABLE((short)1, "账号可用");

    AccountStatus(short status, String description) {
        this.status = status;
        this.description = description;
    }

    private short status;

    private String description;

    public short getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}