package com.java.hohenheim.springplatform.exception;

import com.java.hohenheim.springplatform.define.ResultCodes;

/**
 * @author Hohenheim
 * @date 2019/12/29
 * @description
 */
public class PlatformException extends RuntimeException {
    protected int code;
    protected boolean logging;
    protected String logMsg;

    public PlatformException(ResultCodes result) {
        this(result.getCode(), result.getMsg());
    }

    public PlatformException(int code, String message) {
        this(code, message, message);
    }

    public PlatformException(int code, String message, String logMsg) {
        super(message);
        this.logMsg = logMsg;
        this.code = code;
        logging = true;
    }

    public int getCode() {
        return code;
    }

    public boolean isLogging() {
        return logging;
    }

    public String getLogMsg() {
        return logMsg;
    }
}