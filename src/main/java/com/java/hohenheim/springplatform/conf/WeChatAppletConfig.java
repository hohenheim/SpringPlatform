package com.java.hohenheim.springplatform.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Hohenheim
 * @date 2020/8/11
 * @description
 */
@Component
@ConfigurationProperties("login")
@Data
public class WeChatAppletConfig {
    private String appId;

    private String secret;
}