#!/bin/bash
docker stop spring-platform-1.0.0

docker rm -f spring-platform-1.0.0

docker run -itd --name spring-platform-1.0.0 --privileged=true --net=host --restart=always -v /app/logs:/app/logs -d spring-platform:1.0.0